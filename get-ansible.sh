#!/bin/bash

sudo apt-get install ansible python-pip

pip install --user ansible ansible-tools

export PATH=$HOME/.local/bin:$PATH

echo "Continue with: ansible-local settle.yml -K"
